import 'dart:io';

main() {
  //primera pregunta
  print('¿te gustan las hamburguesas?. Responda "si o no"');
  String preguntaHamburguesa = stdin.readLineSync()!.toLowerCase();

  preguntaHamburguesa = compruebaRespuestaValida(preguntaHamburguesa);

  if (preguntaHamburguesa == "si") {
    // respuesta y segunda pregunta
    print('genial, a mi me encantan las hamburguesas');
    print('¿te gustan los helados? (si o no');
    String preguntaHelado = stdin.readLineSync()!.toLowerCase();

    preguntaHelado = compruebaRespuestaValida(preguntaHelado);

    if (preguntaHelado == "si") {
      //respuesta y tercera pregunta
      print('eres goloso como yo');
      print('¿te gustan las pizzas? (si o no)');
      String preguntaPizza = stdin.readLineSync()!.toLowerCase();

      preguntaPizza = compruebaRespuestaValida(preguntaPizza);

      if (preguntaPizza == "si") {
        print('genial! te gusta todo pero tendrás mala vida, enhorabuena!');
      } else {
        print('si no te gusta la piza eres marica');
      }
    } else {
      print('si no te gusta el helado eres marica');
    }
  } else {
    print('si no te gusta la hamburguesa eres marica');
  }
}

String compruebaRespuestaValida(String respuestaDelUsuario) {
  while (respuestaDelUsuario != "si" && respuestaDelUsuario != "no") {
    print("Por favor, responde con 'si' o 'no'");
    respuestaDelUsuario = stdin.readLineSync()!.toLowerCase();
  }
  return respuestaDelUsuario;
}
