//Exercise 5
// Take two lists, for example:
//
//   a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
//
//   b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
//
// and write a program that returns a list that contains only the
// elements that are common between them (without duplicates). Make
// sure your program works on two lists of different sizes.

void main() {
  final lista1 = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
  final lista2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

  final listaComun = [];
  var yaEstaEnLaEstanteria = [56];

  // dejo la variable listaComun con final preparada entre corchetes
  // para almacenar los números encontrados comunes entre lista1 y lista2

  for (var indiceLista1 = 0; indiceLista1 < lista1.length; indiceLista1 = indiceLista1 + 1) {
    // aquí almaceno en la variable numerosDeLista1 el recorrido hecho en la lista "a" para poder comparar más adelante.
    var numerosDelista1 = lista1[indiceLista1];
    for (var indiceLista2 = 0; indiceLista2 < lista2.length; indiceLista2 = indiceLista2 + 1) {
      var numerosDelista2 = lista2[indiceLista2];
      if (numerosDelista1 == numerosDelista2) {

        yaEstaEnLaEstanteria = [];

        for (var indiceLista3 = 0; indiceLista3 < listaComun.length; indiceLista3 = indiceLista3 + 1) {
          if (numerosDelista1 == listaComun[indiceLista3]) {
            yaEstaEnLaEstanteria.add(numerosDelista1);
          }
        }
        if (yaEstaEnLaEstanteria.isEmpty) {
          listaComun.add(numerosDelista1);
        }
      }
    }
  }
  print(listaComun);
}
