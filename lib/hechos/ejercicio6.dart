//Write a program that takes a list of numbers for example
//
// a = [5, 10, 15, 20, 25]
//
// and makes a new list of only the first and last elements of the given list.
// and print all those values

main() {
  final listaDeNumeros = [89, 9978, 778, 41456, 5669111, 111];
  // print(listaDeNumeros[0]);
  // print(listaDeNumeros[listaDeNumeros.length-1]);

  final primeroYUltimo = [
    listaDeNumeros[0],
    listaDeNumeros[listaDeNumeros.length - 1]
  ];

  for (int posicion = 0; posicion <= primeroYUltimo.length - 1; posicion = posicion + 1) {
    print(primeroYUltimo[posicion]);
  }
}
