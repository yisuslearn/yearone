//Exercise 14
// Write a program (using functions!) that asks the user for a long string
// containing multiple words. Print back to the user the same string,
// except with the words in backwards order.
// For example, say I type the string:
// My name is Michele
// Then I would see the string:
//  a) elehciM si eman yM
//  b) Michele is name My   // " "

import 'dart:io';

void main() {
  print('Escriba una frase:');
  String frase = "My name is Michele"; //stdin.readLineSync()!.toLowerCase();

  // Paso 2: Inicio de variables
  List<String> palabras = [];
  String palabraActual = "";
print('${frase.length}');



  for (int i = frase.length-1; i >= 0; i = i - 1) {

    // print('Capulllo $i');
    stdout.write(frase[i]);

  }
  print('');


  for (int i = 0; i <= frase.length -1; i = i + 1) {

    // print('Capulllo $i');
    stdout.write(frase[frase.length - 1 - i]);

  }
  print('');


  // Paso 3: Iteraro sobre cada carácter de la frase original
  for (int i = 0; i < frase.length; i = i + 1) {
    // Paso 4: Verifico si el carácter actual es un espacio en blanco
    if (frase[i] == ' ') {
      // Paso 5: Agrego la palabra actual a la lista de palabras
      palabras.add(palabraActual);
      // Paso 6: reinicio la variable de palabra actual para contruir sigu. palabra
      palabraActual = "";
    } else {
      // Paso 7: concateno el carácter actual a la palabra actual
      palabraActual = palabraActual + frase[i];
    }
  }

  //Paso 9: agrego la última palabraa la lista de palabras
  palabras.add(palabraActual);

  //Paso 10: invierto orden de palabras para construir frase invertida
  String fraseInvertida = "";
  //Paso 11: recorro palabras de derecha a izquierda.
  for (int i = palabras.length - 1; i >= 0; i = i - 1) {
    //Paso 12: concateno cada palabra invertida a la frase invertida
    fraseInvertida = '$fraseInvertida${palabras[i]} ';
  }

  print(fraseInvertida);
//Paso 13: Invierto el orden de las letras en cada palabra de fraseInvertida
  String fraseAdicional = "";
  for (int i = 0; i < palabras.length; i = i + 1) {
    String palabraInvertida = "";
    for (int j = palabras[i].length - 1; j >= 0; j = j - 1) {
      palabraInvertida = palabraInvertida + palabras[i][j];
    }
    fraseAdicional = '$palabraInvertida $fraseAdicional';
  }
  print(fraseAdicional);
}
