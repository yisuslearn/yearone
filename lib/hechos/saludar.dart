import '../phone/persona.dart';
import '../phone/phone.dart';

void main() {//uso de la clase persona en la función main
  Persona juan = Persona("Juan", 25, 1.70, Phone('gigantesca', 'a tope', 'infinitas'));
  Persona pepe = Persona("Pepe", 37, 1.80, Phone('pequeña', 'deficiente', 'infinitas'));
  juan.saludar();// Imprime: "Hola, mi nombre es Pepe y tengo 37 años."
  pepe.saludar();// Imprime: "Hola, mi nombre es Juan y tengo 25 años."

  juan.llamarA('Manolo');
}
