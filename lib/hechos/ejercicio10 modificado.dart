//Exercise 6
//
// Ask the user for a string and print out whether this string is a palindrome or not.
//
//     A palindrome is a string that reads the same forwards and backwards.

// SACAME EL CALCULO DE SI ES O NO PALINDROMO A UNA FUNCION SEPARADA
// PILLA INSPIRACION DE CHISTACO3MODIFICADO

import 'dart:io';

void main() {

  print('escribe una palabra:');
  String palabra = stdin.readLineSync()!.toLowerCase();
//vamos a obtener la longitud de la cadena y la almacenamos en la variable palabra.
  bool esPalindromo = true;

  esPalindromo = calculoPalindromo(palabra);

  if (esPalindromo == true) {
    print("es palindormo");
  } else {
    print("no los es cabesa");
  }
  // print('$palabra $resultado');
}


bool calculoPalindromo(String palabra) {
  int longitud=palabra.length;
  bool resultado = true;
  for (int i = 0; i < longitud && resultado; i = i + 1) {
    if (palabra[i] != palabra[longitud - 1 - i]) {
      resultado = false;
    }
  }
  return resultado;
}