//Exercise 10
//
// Ask the user for a number and determine whether the number is prime or not.
//HUMILDE CONSEJO: HACERLO PRIMERO, Y LUEGO EXTRAER LA FUNCIÓN.
//     Do it using a function
import 'dart:io';

void main() {
  print('Ingresa un número para ver si es primo:');
  final numero = int.parse(stdin.readLineSync()!);

  bool esPrimo = calculaSiEsPrimo(numero);

  if (esPrimo) {
    print('$numero es un número primo.');
  } else {
    print('$numero no es un número primo.');
  }
}

bool calculaSiEsPrimo(int numero) {
  numero=numero+1;
  print(numero);
  if (numero <=1) {
    return false;
  }
  for (int i = 2; i < numero; i = i + 1 ) {
    if (numero % i == 0) {
      return false;
    }
  }
  return true;
}