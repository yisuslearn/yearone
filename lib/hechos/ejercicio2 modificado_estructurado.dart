import "dart:io";

//hacerlo mismo, pero sin bucle infinito (NO es solo quitar while(true)

void main() {
  //print('imprimir por pantalla si un número es par o impar');

      print(
        'Introduzca bajo esta línea un número para saber si es par o impar. Para terminar el programa, introduzca el valor 0');
    var numeroAConsultar = int.parse(stdin.readLineSync()!);

   while (numeroAConsultar != 0){
     final resto = numeroAConsultar % 2;
     if (resto == 0) {
       print("Este número es par");
     } else {
       print("Este número es impar");
     }
     print("Introduce bajo esta línea un número para saber si es par o impar. Para terminar el programa, introduzca el valor 0");
     numeroAConsultar = int.parse(stdin.readLineSync()!);
   }
   print("programa detenido, aunque el 0 para los matemáticos, es par :D ");
   }
