import 'dart:io';
import 'dart:math';
void main() {
  print('HOla! Acabo de elegir un número del 1 al 100, ¿de qué número se trata?');

  int numeroElegido = int.parse(stdin.readLineSync()!);
int numeroDeIntentos = 1;
  var random = Random();
  var numeroAleatorio = random.nextInt(100)+1;

  while (numeroElegido != numeroAleatorio) {
    if (numeroElegido < numeroAleatorio) {
      print('El número que elegiste es menor que el número aleatorio.');
    } else {
      print('El número que elegiste es mayor que el número aleatorio.');
    }

    print('Inténtalo de nuevo:');
    numeroElegido = int.parse(stdin.readLineSync()!);
    numeroDeIntentos = numeroDeIntentos + 1;
  }

  print('¡Has adivinado! El número correcto es $numeroAleatorio.');
  print(' EL número de intentos es $numeroDeIntentos');
}

