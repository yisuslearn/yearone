//Recite the lyrics to that beloved classic, that field-trip favorite: 99 Bottles of Beer on the Wall.
//
// Note that not all verses are identical.
//
// 99 bottles of beer on the wall, 99 bottles of beer.
// Take one down and pass it around, 98 bottles of beer on the wall.
//
// 98 bottles of beer on the wall, 98 bottles of beer.
// Take one down and pass it around, 97 bottles of beer on the wall.

void main() {
  for (int i = 99; i > 0; i--) {
    if (i == 1) {
      print('$i bottle of beer on the wall, $i bottle of beer ');
    } else {
      print('$i bottles of beer on the wall, $i bottles of beer ');
      print(
          'Take one down and pass it around, ${i - 1} bottles of beer on the wall.');
    }
  }
}
