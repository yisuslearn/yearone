main() {
  final portadaWeb = 'hola mundo isra';
  print(portadaWeb);
  print("hola mundo por isra versión 2");

  print(
      "en la siguiente líean vamos a hacer una suma de dos dígitos enteros, 5 y 7 usando la función int");
  var carretera = 5;
  var marmol = 7;
  var result = carretera + marmol;
  var resultMasUno = result + 1;

  print('El resultado de la suma es: $resultMasUno');
  print('El resultado de la suma es: $result');
  print('El resultado de la suma es: ${carretera + marmol}');

  String frase1 = "Cerveza más paja ";
  String frase2 = "y pa la caja";
  print(frase1 + frase2);

  String nombre = "Juan";
  String saludo = "HOla " + nombre;
  print(saludo);
  String mensaje = "Bienvenido a mi aplicación";
  String mensajeMayusculas = mensaje.toUpperCase();
  print(mensajeMayusculas);
  String mensaje2 =
      "ANTES HE PUESTO TODO EN MAYÚSCULA Y AHORA QUIERO PONER ESTE MENSAJE EN MINÚSCULA";
  String mensajeMinuscula = mensaje2.toLowerCase();
  print(mensajeMinuscula);
  print(
      "Y continuación voy a crear un texto a partir de una lista de caracteres");
  List<String> caracteres1 = [
    "P",
    "R",
    "U",
    "E",
    "B",
    "A",
    " ",
  ];
  String cadenadesaludo1 = caracteres1.join();
  print(cadenadesaludo1);
  List<String> caracteres2 = [
    "S",
    "E",
    "P",
    "A",
    "R",
    "A",
    "D",
    "A",
  ];
  String cadenadesaludo2 = caracteres2.join(" ");
  print(cadenadesaludo2);
  String sumadedospalabrasanteriores = cadenadesaludo1 + cadenadesaludo2;
  print(sumadedospalabrasanteriores);
  int visitasAlWcPromedio = 2;
  print(
      'Vistas al WC Promedio diário usando solo función INT: $visitasAlWcPromedio');
  double visitasAlWvPromedioConDecimales = 2.5;
  print(
      'Vistas al WC Promedio diário usando decimales: $visitasAlWvPromedioConDecimales');
  double meadasPromedioConDecimales = 5.7;
  print('Visitas de meadas con decimales: $meadasPromedioConDecimales');
  double result2 = visitasAlWvPromedioConDecimales + meadasPromedioConDecimales;
  print('el resultado de la suma de deposiciones con decimales es: $result2');
  print("lo que quiero imprimir es cadena de saludo 1 y 2: {$cadenadesaludo1 + $cadenadesaludo2}");
}
