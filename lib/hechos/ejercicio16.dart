void main() {
  List<int> listaOriginal = [1,2,5,1,50,5,57,59,60,80,57,90,95,80];
  List<int> listaSinDuplicados = [];

  listaSinDuplicados = aislarLosDuplicados(List.from(listaOriginal));

  print(listaSinDuplicados);

}

List<int> aislarLosDuplicados(List<int> listaOriginal){
  List<int> resultado = [];
  for (int i = 0; i < listaOriginal.length; i = i + 1) {
    int elementoListaOriginal = listaOriginal[i];
    bool elementoDuplicado = false;
    for (int j = 0; j < i; j = j + 1) {
      if (listaOriginal[j] == elementoListaOriginal) {
        elementoDuplicado = true;
      }
    }
    if (elementoDuplicado == true) {
      resultado.add(elementoListaOriginal);
    }
  }
  return resultado;
}