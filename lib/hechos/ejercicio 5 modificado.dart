import 'dart:io';

void main() {
  stdout.write("Ingresa tu nombre: ");
  String nombreDelUsuario = stdin.readLineSync()!;

  double? edadDelUsuario;
  do {
    stdout.write("Ingresa tu edad: ");
    String edadInput = stdin.readLineSync()!;

    try {
      edadDelUsuario = double.parse(edadInput);
    } catch (e) {
      print("Debe escribir un dato numérico.");
      continue;  // Vuelve al inicio del bucle para pedir otro dato
    }

    if (edadDelUsuario >= 100) {
      print("¡Enhorabuena, $nombreDelUsuario has superado los 100 años!");
    } else {
      double calculoHasta100 = 100 - edadDelUsuario;
      print("¡Hola, $nombreDelUsuario! Te faltan $calculoHasta100 años para cumplir 100 años.");
    }
  } while (edadDelUsuario == null);
}
