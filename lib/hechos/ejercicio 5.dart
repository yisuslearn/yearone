import 'dart:io';

void main() {
  print("Ingresa tu nombre: ");
  String nombreDelUsuario = stdin.readLineSync()!;

  print("Ingresa tu edad: ");
  int edadDelUsuario = int.parse(stdin.readLineSync()!);

  int calculoHasta100 = 100 - edadDelUsuario;

  print("¡Hola, $nombreDelUsuario! Te faltan $calculoHasta100 años para cumplir 100 años.");
}
