void main() {
  print("Vamos a sacar por pantalla los menores que 5 y mayores que 5 usando solo una vez FOR");
  final numeros = [1, 1, 78, 3, 5, 8, 13, 2, 34, 55, 3];
  final menoresQueCinco = [];
  final mayoresQueCinco = [];

  for (int index = 0; index < numeros.length; index = index + 1) {
    final numero = numeros[index];

    if (numero < 5) {
      menoresQueCinco.add(numero);
    } else if (numero > 5) {
      mayoresQueCinco.add(numero);
    }
  }
  print("Números menores que 5: $menoresQueCinco");
  print("Números mayores que 5: $mayoresQueCinco");
}
