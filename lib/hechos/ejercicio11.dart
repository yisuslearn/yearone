//Exercise 7
//
// Let’s say you are given a list saved in a variable:
//
// a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
//
// Write a Dart code that takes this list and makes a new list that
// has only the even elements of this list in it.

void main() {
  final listaPrimaria = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100];
  final listaNueva = [];

  for (int i = 0; i < listaPrimaria.length; i = i + 1) {
    if (listaPrimaria[i] % 2 == 0) {
      listaNueva.add(listaPrimaria[i]);
    }
  }

  print('Lista original: $listaPrimaria');
  print('Lista de elementos pares: $listaNueva');
}
