class Student {
  String nombre;
  int dni;

  Student(this.nombre, this.dni);

  String toString() {
    return "Nombre $nombre - Dni $dni";
  }

  bool esDniPar() {
    return dni % 2 == 0;
  }
}
