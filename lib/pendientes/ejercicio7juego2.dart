import 'dart:io';

void main() {
  // Solicito al usuario el tamaño del tablero
  print('Escribe el tamaño del tablero:');
  int size = int.parse(stdin.readLineSync()!);

  // Defino las cadenas de texto para las líneas horizontales y verticales del tablero
  String lineaHorizontal = ' ' + '-' * 7 + ' ';
  String lineaVertical = '|' + ' ' * 7 + '|';

  // Recorro las filas del tablero
  for (int fila = 0; fila < size; fila = fila + 1) {
    // Imprimo la línea horizontal del tablero
    print(lineaHorizontal * size);

    // Recorro las líneas verticales dentro de cada fila
    for (int linea = 0; linea < 2; linea = linea + 1) {
      // Imprimo la línea vertical del tablero
      print(lineaVertical * size);
    }
  }

  // Imprimo la última línea horizontal del tablero
  print(lineaHorizontal * size);
}
