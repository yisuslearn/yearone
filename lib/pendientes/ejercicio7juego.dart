import 'dart:io';

void main() {
  print("Pon el tamaño del tablero de juego:");
  final cuadradito = int.parse(stdin.readLineSync()!);

  for (int fila = 0; fila < cuadradito; fila = fila + 1) {
  print(" -------" * cuadradito);
  print("|       " * (cuadradito + 1));
  print("|       " * (cuadradito + 1));
  }
  print(" -------" * cuadradito);
}
