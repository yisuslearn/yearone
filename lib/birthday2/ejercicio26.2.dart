import 'dart:io';

import 'package:holamundo/birthday2/birthday2.dart';

void main() {
  final baseDeDatosCumples2 = File("diccionario_de_cumples2");
  baseDeDatosCumples2.createSync(); // Crea el archivo si no existe

  final List<String> cumples = baseDeDatosCumples2.readAsLinesSync();

  Map<String, Birthday2> cumplesPorNombre2 = {};

  for (int i = 0; i < cumples.length; i++) {
    String cumple = cumples[i];
    List<String> atributosDelCumple = cumple.split(";");
    final fecha = atributosDelCumple[0];
    final nombre = atributosDelCumple[1];
    final Birthday2 cumpleConObjeto = Birthday2(fecha, nombre);
    cumplesPorNombre2.putIfAbsent(nombre.toLowerCase(), () => cumpleConObjeto);
  }

  print("");
  while (true) {
    print("Nombres almacenados");
    print("");
    Iterable<String> keys = cumplesPorNombre2.keys;
    for (int i = 0; i < keys.length; i++) {
      print(keys.elementAt(i));
    }
    print("Escriba el nombre de la persona que quiera para saber su fecha de cumpleaños");
    String nombreAConsultar = stdin.readLineSync()!.toLowerCase();

    if (cumplesPorNombre2.containsKey(nombreAConsultar)) {
      print(cumplesPorNombre2[nombreAConsultar]);
    } else {
      print("El nombre no se encuentra en la base de datos.");
    }

    print("¿Desea agregar otro cumpleaños? (s/n)");
    String respuesta = stdin.readLineSync()!.toLowerCase();
    if (respuesta != "s") {
      break;
    }

    print("nombre: ");
    final String nombre = stdin.readLineSync()!;
    print("fecha (xx/yy/bbbb): ");
    final String fecha = stdin.readLineSync()!;

    final Birthday2 birthday2 = Birthday2(fecha, nombre);

    cumplesPorNombre2.putIfAbsent(nombre.toLowerCase(), () => birthday2);

    baseDeDatosCumples2.writeAsString("${birthday2.fecha};${birthday2.nombre}\n", mode: FileMode.append);

    print("Gracias por introducir el cumpleaños de $nombre");
  }
}
