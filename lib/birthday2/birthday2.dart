class Birthday2 {

  String fecha;
  String nombre;

  Birthday2(this.fecha, this.nombre);

  @override
  String toString() {
    return 'El cumpleaños de $nombre es el $fecha';
  }
}