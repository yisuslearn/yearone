class Intentos {
  List<String> letrasMalas;

  Intentos(this.letrasMalas);

  void guardarFallo(String letraFallida) {
    letrasMalas.add(letraFallida);
  }

  @override
  String toString() {
    return 'Fallos: ${letrasMalas.length} => $letrasMalas';
  }
}
