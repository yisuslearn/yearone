import 'dart:io';
import 'intentos.dart';
import 'resultado.dart';

void main() {
  Resultado solucion = Resultado('lentas', []);
  Intentos intentosActuales = Intentos([]);

  print(solucion.palabraEscogida);
  solucion.elijoPalabraAleatoria();
  solucion.muestraElNumeroDeLetrasDeLaSolucion();

  while (solucion.noAveriguada()) {
    print("¿Que letra eliges?");
    String letraEscogida = stdin.readLineSync()!;

    bool esta = solucion.compruebaLaLetra(letraEscogida);

    if (!esta) {
      intentosActuales.guardarFallo(letraEscogida);
    }

    print(intentosActuales);
  }
}
