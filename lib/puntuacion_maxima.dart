class PuntuacionMaxima {
  int puntuacion;
  String nombre;

  PuntuacionMaxima(this.nombre, this.puntuacion);

  String toString() {
    return "Nombre: $nombre - Puntuación: $puntuacion";
  }
}
