//Manage a game player's High Score list.
//
// Your task is to build a high-score component of the classic Frogger game,
// one of the highest selling and most addictive games of all time, and a
// classic of the arcade era. Your task is to write methods that return the
// highest score from the list, the last added score and the three highest scores.
import 'dart:io';
import '../ejercicio21.dart';

void main() {
  List<int> puntuajes = []; //Lista y para almacenar puntuajes (eso está claro)

  bool continuar = true; //Variable booleana para controlar la ejecución del juego

  while (continuar) {

    print("-----------Menú de gestión Frogger -----------");
    print("1. Agregar puntaje");
    print("2. Mostrar último puntaje");
    print("3. Mostrar tres puntuajes más altos");
    print("4. Mostrar puntuaje más alto");
    print("5. Mostrar todos los puntuajes");
    print("6. Salir");
    print("------------------ Frogger -------------------");
    print("Elige una opción");

    int opcion = int.parse(stdin.readLineSync()!); //LEctura de opción seleccionada

    if (opcion == 1) {
      // Paso 1. Agregar puntuaje
      print("introduce el puntuaje");
      int puntuaje = int.parse(stdin.readLineSync()!);
      puntuajes = addInOrder(puntuajes, puntuaje);
      print("puntaje agregado, gracias!");
      print('Elementos $puntuajes');
    } else if (opcion == 2) {
      // Paso 2. Motrar el último puntuaje.
      if (puntuajes.isEmpty) {
        print("no se ha registrado ninguna puntuación");
      } else {
        int ultimoPuntuaje = puntuajes[puntuajes.length - 1];
        print("Último puntuje agregado: $ultimoPuntuaje");
      }
    } else if (opcion == 3) {
      if (puntuajes.isEmpty) {
        print("no se ha registrado ninguna puntuación");
      } else {
        calcularLosTresMasGrandes(puntuajes);
      }
    } else if (opcion == 4) {
      //Paso 4, mostrar puntuaje más alto.
      if (puntuajes.isEmpty) {
        print("no hay puntuajes registrados");
      } else {
        // Localizar puntuaje más alto
        int puntuajeMasAlto = puntuajes[0]; //Si la lista de puntajes no está
        // vacía, se establece la variable puntajeMasAlto inicialmente con el
        // primer puntaje de la lista (puntajes[0]).
        for (int i = 0; i < puntuajes.length; i++) {
          //iteramos sobre lista puntuajes
          int puntuaje = puntuajes[i]; //almacenamos en puntuaje
          if (puntuaje > puntuajeMasAlto) {
            //para puntuaje se compara con puntuajeMasAlto actual
            puntuajeMasAlto = puntuaje; //Si el puntaje actual es mayor que el
            // puntajeMasAlto, se actualiza el valor de puntajeMasAlto con el puntaje actual.
          }
        }
        print("El puntuaje más alto es $puntuajeMasAlto");
      }
    } else if (opcion == 5) {
      // Paso 5, mostrar todos los puntuajes
      if (puntuajes.isEmpty) {
        print("no hay puntuajes registrados");
      } else {
        print("Todas las puntiaciones: $puntuajes");
      }
    } else if (opcion == 6) {
      // Paso 6, salir de la aplicación
      print("Gracias por usar este programa");
      continuar = false; //CIerro bucle while a través de la variable booleana.
    } else {
      print("Por favor, elija una opción del 1 al 6");
    }
  }
}

void calcularLosTresMasGrandes(List<int> todasPuntuaciones) {
  List<int> listaOrdenada = sortList(todasPuntuaciones);
  final elmasgrande = listaOrdenada[0];
  print(elmasgrande);
  print(listaOrdenada[1]);
  print(listaOrdenada[2]);
}

List<int> addInOrder(List<int> puntuajes, int puntuaje) {
  List<int> resultadoListaOrdenada = [];

  resultadoListaOrdenada.add(puntuaje);


  return resultadoListaOrdenada;
}
