class Puntuacion {
  String nombre;
  int puntos;

  Puntuacion(this.nombre, this.puntos);

  @override
  String toString() {
    return '$nombre => $puntos';
  }
}
