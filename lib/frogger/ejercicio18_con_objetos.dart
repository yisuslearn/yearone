//Manage a game player's High Score list.
//
// Your task is to build a high-score component of the classic Frogger game,
// one of the highest selling and most addictive games of all time, and a
// classic of the arcade era. Your task is to write methods that return the
// highest score from the list, the last added score and the three highest scores.
import 'dart:io';
import 'package:holamundo/frogger/puntaciones.dart';

import 'puntuacion.dart';

void main() {
  Puntuaciones puntuacionesActuales = Puntuaciones(<Puntuacion>[]);

  bool continuar = true; //Variable booleana para controlar la ejecución del juego

  while (continuar) {
    print("-----------Menú de gestión Frogger -----------");
    print("1. Agregar puntaje");
    print("2. Mostrar último puntaje");
    print("3. Mostrar tres puntuaciones más altos");
    print("4. Mostrar puntuaje más alto");
    print("5. Mostrar todos los puntuaciones");
    print("6. Salir");
    print("------------------ Frogger -------------------");
    print("Elige una opción");

    int opcion = int.parse(stdin.readLineSync()!); //LEctura de opción seleccionada

    if (opcion == 1) {
      // Paso 1. Agregar puntuaje
      print("introduce tu nombre");
      String nombre = stdin.readLineSync()!;
      print("introduce el puntuaje");
      int puntuaje = int.parse(stdin.readLineSync()!);

      Puntuacion puntuacionActual = Puntuacion(nombre, puntuaje);

      puntuacionesActuales.savePuntuacion(puntuacionActual);


      print('$puntuacionesActuales');
    } else if (opcion == 2) {
      puntuacionesActuales.imprimeLaUltimaPuntuacion();
    } else if (opcion == 3) {
      puntuacionesActuales.imprimeLosTresMasGrandes();
    } else if (opcion == 4) {
      puntuacionesActuales.imprimeElMasAlto();
    } else if (opcion == 5) {
      puntuacionesActuales.imprimeTodasLasPuntuaciones();
    } else if (opcion == 6) {
      // Paso 6, salir de la aplicación
      print("Gracias por usar este programa");
      continuar = false; //CIerro bucle while a través de la variable booleana.
    } else {
      print("Por favor, elija una opción del 1 al 6");
    }
  }
}
