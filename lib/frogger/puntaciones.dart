import 'puntuacion.dart';

class Puntuaciones {
  List<Puntuacion> top;

  Puntuaciones(this.top);

  void savePuntuacion(Puntuacion aGuardar) {
    top.add(aGuardar);
  }

  void imprimeLaUltimaPuntuacion() {
    if (top.isEmpty) {
      print("no se ha registrado ninguna puntuación");
    } else {
      Puntuacion ultimaPuntuacion = top[top.length - 1];
      print("Última puntuación agregada");
      print('$ultimaPuntuacion');
    }
  }

  void imprimeLosTresMasGrandes() {
    if (top.isEmpty) {
      print("no se ha registrado ninguna puntuación");
    } else {
      calcularLosTresMasGrandes(top);
    }
  }

  void calcularLosTresMasGrandes(top) {
    if (top.length < 3) {
      print("No hay suficientes puntuaciones registradas.");
    } else {
      List<Puntuacion> listaCopiada = List.from(top); //copio lista para evitar modificar orden original
      for (int i = 0; i < 3; i++) {
        int maxIndex = i; //asumo que el elemento en la posicion i es el máximo dentro del rango no ordenado
        for (int j = i; j < listaCopiada.length; j++) {
          if (listaCopiada[j].puntos > listaCopiada[maxIndex].puntos) {
            maxIndex = j;
          }
        }
        Puntuacion temp = listaCopiada[i];
        listaCopiada[i] = listaCopiada[maxIndex];
        listaCopiada[maxIndex] = temp;
      }
      print("Las tres puntuaciones más altas son:");
      for (int i = 0; i < 3; i++) {
        print("${i + 1}  => ${listaCopiada[i].toString()}");
      }
    }
  }

  void imprimeElMasAlto() {
    if (top.isEmpty) {
      print("no hay puntuaciones registradas");
    } else {
      Puntuacion puntuacionMasAlta = top[0];
      for (int i = 1; i < top.length; i++) {
        if (top[i].puntos > puntuacionMasAlta.puntos) {
          puntuacionMasAlta = top[i];
        }
      }
      print("El puntaje más alto es $puntuacionMasAlta");
    }
  }

  void imprimeTodasLasPuntuaciones() {
    if (top.isEmpty) {
      print("no hay puntuaciones registrados");
    } else {
      print(toString());
    }
  }

  @override
  String toString() {
    String resultado = "Todas las puntuaciones: \n";
    for (int i = 0; i < top.length; i++) {
      Puntuacion puntuacion = top[i];
      resultado = '$resultado$puntuacion\n';
    }
    return resultado;
  }
}
