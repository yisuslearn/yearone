class ParOImpar {
  int numero;

  ParOImpar(this.numero);

  void esPar() {
    final int resto = numero % 2;
    if (resto == 0) {
      print("este número es par");
    } else {
      print("este número es impar");
    }
    print("Consulte otro número o introduzca 0 para salir del programa");
  }
}
