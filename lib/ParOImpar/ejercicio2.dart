 import "dart:io";

void main() {
  //print('imprimir por pantalla si un número es par o impar');
  print('Veamos si un número es par o impar');
  while (true) {
    //bucle infinito

    var numeroAConsultar = int.parse(stdin.readLineSync()!);

    if (numeroAConsultar == 0) {
      //condición para salir del bucle
      print("Programa detenido");
      break; //instrucción para salir del bucle
    }

    final int resto = numeroAConsultar % 2;
    if (resto == 0) {
      print("este número es par");
      print("Consulte otro número o introduzca 0 para salir del programa");
    } else {
      print("este número es impar");
      print("Consulte otro número o introduzca 0 para salir del programa");
    }
  }
}
