import "dart:io";

import "package:holamundo/ParOImpar/calculo_par_impar.dart";

void main() {
  //print('imprimir por pantalla si un número es par o impar');
  print('Veamos si un número es par o impar');
  while (true) {
    //bucle infinito

    var numeroAConsultar = int.parse(stdin.readLineSync()!);

    if (numeroAConsultar == 0) {
      //condición para salir del bucle
      print("Programa detenido");
      break; //instrucción para salir del bucle
    }

    ParOImpar numero = ParOImpar(numeroAConsultar);
    numero.esPar();
  }
}
