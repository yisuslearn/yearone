//Exercise 26
//
// For this exercise, we will keep track of when our friend’s birthdays are, and be able to find that information based on their name.
//
// Create a dictionary (in your file) of names and birthdays. When you run your program it should ask the user to
// enter a name, and return the birthday of that person back to them. The interaction should look something like this:
//
// >>> Welcome to the birthday dictionary. We know the birthdays of:
// Albert Einstein
// Benjamin Franklin
// Ada Lovelace
// >>> Who's birthday do you want to look up?
// Benjamin Franklin
// >>> Benjamin Franklin's birthday is 01/17/1706.

//  APUNTES
// import 'package:holamundo/frogger/puntuacion.dart';
//
// void main() {
//   Map<String, Puntuacion> puntuacionesPorNombre = {};
//   // Map<String, Puntuacion> puntuacionesPorNombre2 = {
//   //   "peputa": Puntuacion("peputa", 56),
//   // };
//
//   List<Puntuacion> listaDePuntuaciones = [];
//
// //   {
// //          "maNolo": Puntacion("manolon", 34),
// //          "peputa": Puntuacion("peputa", 56),
// //   }
// //
// //   [
// //         Puntuacion("manolon", 34),
// //         Puntuacion("peputa", 56),
// //   ]
//
//   listaDePuntuaciones.add(Puntuacion("peputa", 56));
//   // listaDePuntuaciones[0] = Puntuacion("peputa", 56);
//
//   puntuacionesPorNombre.putIfAbsent('peputa', () => Puntuacion("peputa", 56));
// }
import 'dart:io';
import 'package:holamundo/birthdays/birthday.dart';

void main() {
  final baseDeDatosDeCumples = File('diccionarios_de_cumples.txt');

  // print(baseDeDatosDeCumples..absolute);
  //
  // String contenidoDelFichero = baseDeDatosDeCumples.readAsStringSync();
  //
  // print(contenidoDelFichero);

  // baseDeDatosDeCumples.writeAsStringSync('Isra es el mejor\n', mode: FileMode.append);

  // print(baseDeDatosDeCumples.readAsLinesSync());

  // List<String> esu = "caca;pe     do".split(';');
  // print(esu);

  Map<String, Birthday> cumplesPorNombre = {};


  final List<String> cumples = baseDeDatosDeCumples.readAsLinesSync();

  for (int i = 0; i < cumples.length; i++) {
    String cumple = cumples[i];
    List<String>  atributosDelCumple = cumple.split(';');
    final fecha = atributosDelCumple[0];
    final nombre = atributosDelCumple[1];

    final Birthday cumpleComoObjeto = Birthday(fecha, nombre);

    cumplesPorNombre.putIfAbsent(nombre.toLowerCase(), () => cumpleComoObjeto);
  }


  // cumplesPorNombre.putIfAbsent("jesus", () => Birthday("03/07/1986", "Jesús"));
  //
  // cumplesPorNombre.putIfAbsent("laura", () => Birthday("01/01/1986", "Laura"));

  Iterable<String> keys = cumplesPorNombre.keys;
  for (int i = 0; i < keys.length; i++) {
    print(keys.elementAt(i));
  }

  print("Escribe el nombre de la persona que quieras consultar el cumpleaños:");
  String nombreAConsultar = stdin.readLineSync()!.toLowerCase();

  print(cumplesPorNombre[nombreAConsultar]);

  print("y ahora me vas a neter otro cumple");
  print("nombre: ");
  final String nombre = stdin.readLineSync()!;

  print("fecha (xx/yy/bbbb): ");
  final String fecha = stdin.readLineSync()!;

  final Birthday birthday = Birthday(fecha, nombre);

  cumplesPorNombre.putIfAbsent(nombre.toLowerCase(), () => birthday);


  baseDeDatosDeCumples.writeAsStringSync('${birthday.fecha};${birthday.nombre}\n', mode: FileMode.append);


  print(cumplesPorNombre);
}
