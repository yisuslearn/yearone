class Birthday {

  String fecha;
  String nombre;

  Birthday(this.fecha, this.nombre);

@override
  String toString() {
        return "El cumpleaños de $nombre es el $fecha";
  }

}