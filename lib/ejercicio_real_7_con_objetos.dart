//Let’s say you are given a list saved in a variable:
//
// a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
//
// Write a Dart code that takes this list and makes a new list that has only
// the even elements of this list in it.

// pero con la clase Student que tiene nombre y dni (el dni son los valores de la lista original)
// y la clase Classroom donde tiene que tener el listado de estudiantes que van
import 'student.dart';

void main() {
  final listaEstudiantes = [
    Student('andre', 1),
    Student('pepe', 4),
    Student('maria', 9),
    Student('ange', 16),
    Student('manolo', 25),
    Student('isra', 36),
    Student('trisha', 49),
    Student('leo', 64),
    Student('eva', 81),
    Student('ana', 100)
  ];
  print(listaEstudiantes);
  List<Student> listaDePares = [];

  for (int i = 0; i < listaEstudiantes.length; i++) {
    var estudiante = listaEstudiantes[i];
    if (estudiante.esDniPar()) {
      listaDePares.add(estudiante);
    }
  }
  print(listaDePares);
}
