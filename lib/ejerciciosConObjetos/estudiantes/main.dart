import 'package:holamundo/ejerciciosConObjetos/estudiantes/estudiante.dart';
import 'package:holamundo/ejerciciosConObjetos/estudiantes/persona.dart';
import 'package:holamundo/ejerciciosConObjetos/estudiantes/profesor.dart';

void main(){
  var persona1 = Persona('JUan', 30,direccion: "calle calle", telefono: "+34");
  var estudiante1 = Estudiante("María", 30, "US",direccion: "Calle Tal", telefono: "6565324");

  List<Persona> personas = [Persona("Juan", 30, direccion: "Calle olmo", telefono: "656565656"),
  Estudiante("María", 30, "Olavide",telefono: "000", direccion: "urbanizacion"),Estudiante("Pedro", 50, "LASORBONA",direccion: "Calle Prado",telefono: "666999777"),
  Profesor("Ana", 45, "Matemáticas", 5000,direccion: "calle olvidada",telefono: "personal")];
  

  for (int i = 0; i < personas.length; i = i +1){
    print("información de la persona: ");
    var persona = personas[i];
    persona.mostrarInformacion();
    print("-------------------------");

  }

  print("Información de la persona:");
  persona1.mostrarInformacion();
  print("----------------------------------------");

  persona1.establecerDireccion("Nueva dirección");
  persona1.establecerTelefono("+34647754717");

  print("Información actualizada de la persona");
  persona1.mostrarInformacion();

  print("Información del estudiante:");
  estudiante1.mostrarInformacion();
}