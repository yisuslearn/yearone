import 'package:holamundo/ejerciciosConObjetos/estudiantes/persona.dart';

class Profesor extends Persona{

  String materia;
  double salario;

  Profesor(String nombre, int edad,this.materia, this.salario, {String direccion  = "", String telefono = ""})
  : super(nombre,edad, direccion:direccion, telefono:telefono);

  @override

  void mostrarInformacion(){
    print("PROFESOR ");
    super.mostrarInformacion();
    print("Materia: $materia");
    print("Salario: \$${salario.toStringAsFixed(2)}");
  }

}