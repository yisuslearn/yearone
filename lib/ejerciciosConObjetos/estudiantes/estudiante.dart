import 'package:holamundo/ejerciciosConObjetos/estudiantes/persona.dart';

class Estudiante extends Persona{
  String universidad;

  Estudiante(String nombre, int edad, this.universidad, {String direccion = "", String telefono = ""}) : super(nombre,edad, direccion: direccion,telefono: telefono);

  @override
  void mostrarInformacion(){
    super.mostrarInformacion();
    print("Universidad: $universidad");
  }
}