class Persona{
  String nombre;
  int edad;
  String direccion;
  String telefono;


  Persona(this.nombre, this.edad, {this.direccion = "", this.telefono = ""});

  void mostrarInformacion(){
    print("Nombre: $nombre");
    print("Edad: $edad años");
    print("Direccion: $direccion");
    print("Telefono: $telefono");
  }
  void establecerDireccion(String nuevaDireccion){
    direccion = nuevaDireccion;
  }
  void establecerTelefono(String nuevoTelefono){
    telefono = nuevoTelefono;
  }
}