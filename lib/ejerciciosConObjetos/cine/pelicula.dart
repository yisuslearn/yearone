class Pelicula {
  String titulo;
  int duracion;
  int edadMinima;
  String director;

  Pelicula(this.titulo, this.duracion, this.edadMinima, this.director);
}
