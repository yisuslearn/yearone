import 'dart:io';
import 'dart:math';

import 'package:holamundo/ejerciciosConObjetos/cine/espectador.dart';

import 'asiento.dart';
import 'pelicula.dart';

class Cine {
  Pelicula peliculaActual;
  double precioEntrada;
  List<Asiento> asientos = [];

  Cine(this.peliculaActual, this.precioEntrada);

  // void asignarAsiento(Espectador cliente) {
  //   var asientoOcupado = Asiento("etiqueta", cliente);
  //   asientos.add(asientoOcupado);
  // }

  void inicializarSalaParaEtiquetarAsientosYParaIndicarQueNoHayNadie() {
    final filas = 8;
    final letras = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];

    for (int fila = 0; fila < filas; fila = fila + 1) {
      for (int columna = 0; columna < letras.length; columna = columna + 1) {
        final letra = letras[columna];
        final etiqueta = "${fila + 1}$letra";
        final Asiento asientoLibre = Asiento(etiqueta, null);
        asientos.add(asientoLibre);
      }
    }
  }

  void mostrarEstadoAsientos() {
    int asientosLibres = 0;

    for (int i = 0; i < asientos.length; i = i + 1) {
      var etiquetaAsiento = "| ${asientos[i].etiqueta} ";

      if (asientos[i].estaVacio()) {
        etiquetaAsiento = "$etiquetaAsiento  ";
        asientosLibres = asientosLibres + 1;
      } else {
        etiquetaAsiento = "$etiquetaAsiento \$ ";
      }

      stdout.write(etiquetaAsiento);
      if ((i + 1) % 9 == 0) {
        stdout.write("|\n");
      }
    }

    print("\n Quedan $asientosLibres asientos libres");
  }

  void asignarAsiento(Espectador tortilla) {
    final generadorAleatorio = Random();
    bool asientoVacioEncontrado = false;

    for (int i = 0; i < asientos.length && !asientoVacioEncontrado; i = i + 1) {
      var unAsientoCualquiera = generadorAleatorio.nextInt(asientos.length);
      final Asiento butaca = asientos[unAsientoCualquiera];
      if (butaca.estaVacio()) {
        asientoVacioEncontrado = true;

        final butacaAsignada = Asiento(butaca.etiqueta, tortilla); //nuevo obj. con espectador tortilla > inmutab
        asientos[unAsientoCualquiera] = butacaAsignada; //reemplazo asiento vacio aleatorio con el nuevo asiento.
      }
    }
  }
}
