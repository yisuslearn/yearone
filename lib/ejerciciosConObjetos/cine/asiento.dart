import 'espectador.dart';

class Asiento {
  String etiqueta;
  final Espectador? cliente;

  Asiento(this.etiqueta, this.cliente);

  bool estaVacio() {
    return cliente == null;
  }
}
