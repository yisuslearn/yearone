import 'dart:io';
import 'package:holamundo/ejerciciosConObjetos/palindromo/verificadorPalindromo.dart';
import 'menuDeOpciones.dart';

main() {
  bool continuar = true;
  VerificadorPalindromoo verificador = VerificadorPalindromoo([],[]);
  MenuDeOpciones impresion = MenuDeOpciones();

  while (continuar) {

    impresion.mostrarMenu();

    String input = stdin.readLineSync()!;

    if (input == "1") {
      print("Ingresa la palabra a consultar:");
      String palabraConsultada = stdin.readLineSync()!;
      verificador.consultadorDePalabra(palabraConsultada);
    } else if (input == "2") {
      print(verificador);
    } else if (input == "3") {
      verificador.mostrarPalabraPalindromoMasLarga();
    } else if (input == "4") {
      continuar = false;
    } else {
      print("Opción inválida. Por favor, ingresa una opción válida (1-4).");
    }
  }
}

