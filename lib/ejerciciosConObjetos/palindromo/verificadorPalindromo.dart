class VerificadorPalindromoo {
  List<String> listadoDePalabrasNoPalindromo = [];
  List<String> listadoDePalabrasPalindromo = [];

  VerificadorPalindromoo(this.listadoDePalabrasPalindromo,this.listadoDePalabrasNoPalindromo);

  void consultadorDePalabra(String palabraConsultada) {
    bool esPalindromo = analizadorPalindromo(palabraConsultada);
    if (esPalindromo) {
      listadoDePalabrasPalindromo.add(palabraConsultada);
      print("la palabra $palabraConsultada es palíndromo");
    } else {
      listadoDePalabrasNoPalindromo.add(palabraConsultada);
      print("la palabra $palabraConsultada no es palíndromo");
    }
  }

  bool analizadorPalindromo(String palabraConsultada) {
    bool resultado = true;
    int longitudDePalabraConsultada = palabraConsultada.length;
    for (int i = 0; i < longitudDePalabraConsultada && resultado; i++) {
      if (palabraConsultada[i] != palabraConsultada[longitudDePalabraConsultada - 1 - i]) {
        resultado = false;
      }
    }
    return resultado;
  }

  void mostrarResultadosConsultados() {
    print("Palabras palíndromo: $listadoDePalabrasPalindromo");
    print("Palabras No Palíndromo: $listadoDePalabrasNoPalindromo");
  }

  void mostrarPalabraPalindromoMasLarga() {
    String palabraPalindromoMasLarga = listadoDePalabrasPalindromo[0];

    for (int i = 1; i < listadoDePalabrasPalindromo.length; i++){
      String palabraActual = listadoDePalabrasPalindromo[i];
      if (palabraActual.length > palabraPalindromoMasLarga.length){
        palabraPalindromoMasLarga = palabraActual;
      }
    }
    print("La palabra palíndromo $palabraPalindromoMasLarga es la más larga");
  }
  String toString(){
    return "Palabras palíndromo: $listadoDePalabrasPalindromo\n"
        "Palabras No Palíndromo: $listadoDePalabrasNoPalindromo";

  }
}
