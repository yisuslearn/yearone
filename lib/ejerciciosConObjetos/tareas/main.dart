import 'lista_de_tareas.dart';
import 'tarea.dart';

void main() {
  final ListaDeTareas listaDeTareas = ListaDeTareas();

  listaDeTareas.agregarTarea("Hacer la compra");
  listaDeTareas.agregarTarea("Lavar el coche");
  listaDeTareas.agregarTarea("Estudiar dart");

  print("Lista de tareas");
  listaDeTareas.listarTareas();

  Tarea tarea1 = listaDeTareas.tareas[0];
  print(tarea1);
  tarea1.marcarCompletada();
  print(tarea1);

  listaDeTareas.eliminarTarea("Lavar el coche");
  listaDeTareas.eliminarTareaCompletada();
  print("Lista de tareas actualizada");
  listaDeTareas.listarTareas();


}
