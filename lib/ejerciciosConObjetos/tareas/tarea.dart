class Tarea {
  String nombre;
  bool completada;

  Tarea(this.nombre, this.completada);

  void marcarCompletada() {
    completada = true;
  }

  @override
  String toString() {
    if (completada) {
      return "$nombre - Completada";
    } else {
      return "$nombre - Pendiente";
    }
  }
}
