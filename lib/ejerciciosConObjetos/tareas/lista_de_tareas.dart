import 'tarea.dart';

class ListaDeTareas{
  List<Tarea> tareas = [];

  void agregarTarea (String nombre){
    final tarea = Tarea(nombre, false);
    tareas.add(tarea);
  }
  void listarTareas(){
    for (int i = 0; i < tareas.length; i = i +1){
      Tarea tarea = tareas[i];
      print(tarea);
    }
  }
  void eliminarTarea(String nombre){
    for (int i = 0; i < tareas.length; i++) {
      if (tareas[i].nombre == nombre) {
        tareas.removeAt(i);
        print('Tarea "$nombre" eliminada.');
        return; // Salir del método después de eliminar la tarea
      }
    }

    // Si no se encuentra la tarea, imprimir un mensaje
    print('Tarea "$nombre" no encontrada en la lista.');
  }
  void eliminarTareaCompletada(){
    tareas.removeWhere((tarea) => tarea.completada);
    print("TOdas las tareas completadas han sido eliminadas");
  }
}
