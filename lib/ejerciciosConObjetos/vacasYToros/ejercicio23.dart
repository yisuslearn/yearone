//Create a program that will play the “cows and bulls” game with the user. The game works like this:

//Randomly generate a 4-digit number. Ask the user to guess a 4-digit number.
// For every digit the user guessed correctly in the correct place, they have
// a “cow”. For every digit the user guessed correctly in the wrong place is a “bull.”
//Every time the user makes a guess, tell them how many “cows” and “bulls”
// they have. Once the user guesses the correct number, the game is over. Keep
// track of the number of guesses the user makes throughout the game and tell the user at the end.

import 'dart:math';
import 'dart:io';

import 'package:holamundo/ejerciciosConObjetos/vacasYToros/partida_vacas_y_toros.dart';
import 'package:holamundo/ejerciciosConObjetos/vacasYToros/resultado_vacas_y_toros.dart';

import '../../advinaNumero/partida.dart';

void main() {
  print("Este es el juego de Vacas y Toros");
  print("Intenta adivinar el número de 4 dígitos");

  Partida partidaEnCurso = Partida('');

  partidaEnCurso.inicioPartida();

  Resultado resultadoDeLaPartida = Resultado(0, 0, 0);
  bool adivinado = false;

  while (!adivinado) {
    stdout.write('Escribe tu número de 4 dígitos:');
    String input = stdin.readLineSync()!;

    resultadoDeLaPartida.otroIntento();

    for (int i = 0; i < partidaEnCurso.numeroObjetivo.length; i++) {
      if (input[i] == partidaEnCurso.numeroObjetivo[i]) {
        resultadoDeLaPartida.toroEncontrado();
      } else if (partidaEnCurso.numeroObjetivo.contains(input[i])) {
        resultadoDeLaPartida.vacaEncontrada();
      }
    }

    print("Resultado $resultadoDeLaPartida");

    if (input == partidaEnCurso.numeroObjetivo) {
      print("Has acertado ${partidaEnCurso.numeroObjetivo} con ${resultadoDeLaPartida.intentos} intentos");
      adivinado = true;
    } else {
      print("No has acertado, sigue intentándolo");
    }
  }
}
