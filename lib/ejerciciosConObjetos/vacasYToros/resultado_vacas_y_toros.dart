class Resultado {
  int vacas;
  int toros;
  int intentos;

  Resultado(this.vacas, this.toros, this.intentos);

  void toroEncontrado() {
    toros = toros + 1;
  }

  void vacaEncontrada() {
    vacas = vacas + 1;
  }

  void otroIntento() {
    vacas = 0;
    toros = 0;
    intentos = intentos + 1;
  }

  @override
  String toString() {
    return "Vacas $vacas, Toros $toros, Intentos $intentos";
  }
}
