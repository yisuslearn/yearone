import 'cuenta.dart';
import 'persona.dart';

void main() {

  Persona persona1 = Persona("Pepe", 30, "H", 70.0, 1.75);
  String representacionPersona = persona1.toString();
  print(representacionPersona);

  String generadorDni = persona1.generaDni();

  print("Dni Aleatorio: $generadorDni");

  int resultadoImc = persona1.calcularIMC();

  if (resultadoImc == -1) {
    print("La persona tiene un IMC menor a 20, tiene un IMC Bajo");
  } else if (resultadoImc == 0) {
    print("La persona tiene un IMC entre 20 y 25, tiene un IMC normal");
  } else {
    print("La persona tiene un IMC mayor a 25, está en sobrepeso");
  }


}
