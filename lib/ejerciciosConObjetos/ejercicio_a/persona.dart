import 'dart:math';

class Persona {
  String nombre = "";
  int edad = 0;
  String dni = "";
  String sexo = "";
  double peso = 0.0;
  double altura = 0.0;


  Persona(this.nombre, this.edad, this.sexo, this.peso, this.altura){
    dni = generaDni();
  }

   int calcularIMC() {
    double imc = peso / (altura * altura);
    print("El valor del IMC es $imc");
    if (imc < 20){
      return -1; // por debajo de peso ideal
    } else if (imc >= 20 && imc <=25){
      return 0; // peso ideal
    } else {
      return 1; //sobrepeso
    }
  }

  bool esMayorDeEdad() {
    return edad >= 18;
  }

  @override
  String toString() {
    return 'Persona{nombre: $nombre, edad: $edad, dni: $dni, sexo: $sexo, peso: $peso, altura: $altura}';
  }
  String generaDni(){
     var generadorAleatorio = Random();
     int numeroAleatorio = generadorAleatorio.nextInt(90000000) + 10000000;
     List<String> letrasDni = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
     int indiceDeLetra = numeroAleatorio % 23;
     String letraDni = letrasDni[indiceDeLetra];

     return "$numeroAleatorio-$letraDni";
  }

}