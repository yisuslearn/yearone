class Cuenta {
  String titular;
  double cantidad;

  Cuenta(this.titular, {this.cantidad = 0.0});

  String getTitular() {
    return titular;
  }

  double getCantidad() {
    return cantidad;
  }

  void setCantidad(double nuevaCantidad) {
    if (nuevaCantidad >= 0) {
      cantidad = nuevaCantidad;
    }
  }

  @override
  String toString() {
    return 'Cuenta{titular: $titular, cantidad: $cantidad}';
  }

  void ingresarCantidad(double nuevaCantidad) {
    if (nuevaCantidad >= 0) {
      cantidad = cantidad + nuevaCantidad;
    }
  }
  void retirarCantidad(double cantidadRetirada){
    if(cantidadRetirada >= 0){
      if(cantidad - cantidadRetirada < 0){
        cantidad = 0;
      } else {
        cantidad = cantidad - cantidadRetirada;
      }
    }
  }
}
