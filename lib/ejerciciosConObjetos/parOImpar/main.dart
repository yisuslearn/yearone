import 'dart:io';
import 'verificadorParOIMpar.dart';

// verificador tiene que recordar TODOS los valores de UNA ejecucion

void main() {
  final verificadorNumerosParesImpares = VerificadorParOImpar(0, 0, 0, 0,[],[]);

  print(
      "Introduzca el número a consultar. Para terminar el programa introduzca 0. Para ver los totales introduzca -1");
  int numeroAConsultar = int.parse(stdin.readLineSync()!);

  while (numeroAConsultar != 0) {
    if (numeroAConsultar == -1) {
      print(verificadorNumerosParesImpares);
    } else {
      verificadorNumerosParesImpares.verifica(numeroAConsultar);
    }

    print("Introduzca el número a consultar. Para terminar el programa introduzca 0. Para ver los totales introduzca -1");
    numeroAConsultar = int.parse(stdin.readLineSync()!);
  }

  print("Programa detenido, aunque el 0 para los matemáticos, es par :D ");
}
