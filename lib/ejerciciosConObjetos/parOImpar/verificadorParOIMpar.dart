class VerificadorParOImpar {
  int sumaPares;
  int sumaImpares;
  int cantidadPares;
  int cantidadImpares;
  List<int> paresConsultados;
  List<int> imparesConsultados;

  VerificadorParOImpar(this.sumaPares, this.sumaImpares, this.cantidadPares, this.cantidadImpares,
      this.paresConsultados, this.imparesConsultados);

  void verifica(int numeroAConsultar) {
    final resto = numeroAConsultar % 2;
    if (resto == 0) {
      print("$numeroAConsultar es par");
      sumaPares = sumaPares + numeroAConsultar;
      cantidadPares++;
      paresConsultados.add(numeroAConsultar);
      paresConsultados.sort();
    } else {
      print("$numeroAConsultar es impar");
      sumaImpares = sumaImpares + numeroAConsultar;
      cantidadImpares++;
      imparesConsultados.add(numeroAConsultar);
      imparesConsultados.sort();
    }
  }

  @override
  String toString() {
    return "Cantidad de números pares ingresados: $cantidadPares\n"
        "Cantidad de números impares ingresados: $cantidadImpares\n"
        "Suma de números pares ingresados: $sumaPares\n"
        "Suma de números impares ingresados: $sumaImpares\n"
        "Listado de pares consultados: $paresConsultados\n"
        "Listado de impares consultados: $imparesConsultados";
  }

}
