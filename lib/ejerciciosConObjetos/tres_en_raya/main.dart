//  --- --- ---
// |   |   |   |
//  --- --- ---
// |   |   |   |
//  --- --- ---
// |   |   |   |
//  --- --- ---
//
// This one is 3x3 (like in tic tac toe).
//
// Ask the user what size game board they want to draw, and draw it for them to the screen using Dart’s print statement.

import 'dart:io';

import 'package:holamundo/ejerciciosConObjetos/tres_en_raya/jugador.dart';
import 'package:holamundo/ejerciciosConObjetos/tres_en_raya/tablero.dart';

main() {
  print("JUEGO DE TRES EN RAYA");
  int tamanoTablero = 4;
  Tablero tresEnRaya = Tablero(tamanoTablero);
  tresEnRaya.iniciarPartida();

  tresEnRaya.imprimirTablero();

  for (int turno = 0; turno < tresEnRaya.totalMovimientos(); turno = turno + 1) {
    Jugador jugadorActual = tresEnRaya.siguenteJugador(turno);

    print("Jugador $jugadorActual, ¿qué fila quieres usar? Escriba de 1-$tamanoTablero");
    int fila = int.parse(stdin.readLineSync()!) - 1;

    print("Jugador $jugadorActual, ¿qué columna quieres usar? Escriba de 1-$tamanoTablero");
    int columna = int.parse(stdin.readLineSync()!) - 1;

    if (tresEnRaya.estadoDelTablero[fila][columna] == " ") {
      tresEnRaya.jugada(fila, columna, jugadorActual.elemento);
    } else {
      print("La posición ya está ocupada, elija otra posición");
      turno = turno - 1;
    }
  }
}
