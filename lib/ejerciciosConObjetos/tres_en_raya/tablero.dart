import 'package:holamundo/ejerciciosConObjetos/tres_en_raya/jugador.dart';
import 'package:holamundo/ejerciciosConObjetos/tres_en_raya/movimiento.dart';

class Tablero {
  int tamanoDelTablero;
  List<Movimiento> movimientos = [];
  List<List<String>> estadoDelTablero = [];

  Jugador jugadorX = Jugador("X");
  Jugador jugador0 = Jugador("O");

  Tablero(this.tamanoDelTablero);

  void iniciarPartida() {
    for (int fila = 0; fila < tamanoDelTablero; fila = fila + 1) {
      List<String> columnaActual = [];
      for (int columna = 0; columna < tamanoDelTablero; columna = columna + 1) {
        columnaActual.add(' ');
      }
      estadoDelTablero.add(columnaActual);
    }
    print(estadoDelTablero);
  }

  void imprimirEnPosicion() {
    imprimirTablero();
  }

  void imprimirTablero() {
    String separadorHorizontal = "";
    for (int fila = 0; fila < tamanoDelTablero; fila = fila + 1) {
      separadorHorizontal = "";
      for (int linea = 0; linea < tamanoDelTablero; linea = linea + 1) {
        separadorHorizontal = "$separadorHorizontal ---";
      }
      print(separadorHorizontal);

      String celda = "|";
      for (int columna = 0; columna < tamanoDelTablero; columna = columna + 1) {
        // if (fila == movimiento.fila && columna == movimiento.columna) {
        //   celda = "$celda ${movimiento.elemento} |";
        // } else {
        //   celda = "$celda   |";
        // }

        celda = "$celda ${estadoDelTablero[fila][columna]} |";
      }
      print(celda);
    }
    print(separadorHorizontal);
  }

  void jugada(int fila, int columna, String elemento) {
    Movimiento ultimo = Movimiento(fila, columna, elemento);
    movimientos.add(ultimo);

    modificarEstadoTablero(fila, columna, elemento);

    imprimirEnPosicion();
    print('Quedan ${tamanoDelTablero * tamanoDelTablero - movimientos.length} movimientos restantes');
  }


  void modificarEstadoTablero(int fila, int columna, String elemento) {
    // 3. Modifiques el valor de estadoDelTablero con el ELEMENTO de la jugada
    estadoDelTablero[fila][columna] = elemento;
  }

  int totalMovimientos() {
    return tamanoDelTablero * tamanoDelTablero - movimientos.length;
  }

  Jugador siguenteJugador(int turno) {
    if (turno % 2 == 0) {
      return jugador0;
    } else {
      return jugadorX;
    }
  }
}
