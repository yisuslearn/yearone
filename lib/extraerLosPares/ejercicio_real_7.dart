//Let’s say you are given a list saved in a variable:
//
// a = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100].
//
// Write a Dart code that takes this list and makes a new list that has only
// the even elements of this list in it.
import 'extracción_de_numeros_pares.dart';

void main(){
  List<int> listaOriginal = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100];
  print(listaOriginal);
  List<int> listaDePares = [];

  for (int i = 0; i<listaOriginal.length ;i++){
    if (listaOriginal[i] % 2 == 0) {
      listaDePares.add(listaOriginal[i]);
    }
  }
  print(listaDePares);
}