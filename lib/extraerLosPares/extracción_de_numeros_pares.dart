class NumerosPares {
  List<int> listaOriginal;
  late List<int> listaDePares;

  NumerosPares(this.listaOriginal) {
    listaDePares = extraerPares();
  }

  List<int> extraerPares() {
    List<int> pares = [];

    for (int i = 0; i < listaOriginal.length; i++) {
      if (listaOriginal[i] % 2 == 0) {
        pares.add(listaOriginal[i]);
      }
    }

    return pares;
  }
}

