//You, the user, will have in your head a number between 0 and 100.
// The program will guess a number, and you, the user,
// will say whether it is too high, too low, or your number.
//
//     At the end of this exchange, your program should print out
//     how many guesses it took to get your number.

import 'dart:io';
import 'dart:math';
import 'interfaz_de_usuario.dart';

void main() {
  print('HOla! Piense un número del 1 al 100, voy a tratar de averiguarlo');
  print('Preseione enter cuando haya pensado el número');
  stdin.readLineSync()!;

  // num 83
  //82 ==< 2
  // 84 ==< 1
  //peta y deberia decir 83 como respuesta

  var generadorAleatorio = Random();
  var numeroAleatorio = generadorAleatorio.nextInt(100) + 1;
  int numeroDeINtentos = 1;
  InterfazDeUsuario interfaz = InterfazDeUsuario(numeroAleatorio, generadorAleatorio, numeroDeINtentos);

  interfaz.presentaOpcionesAlUsuario();

  int respuestaDelUsuario;
  bool adivinado = false;
  int limiteInferior = 1;
  int limiteSuperior = 100;

  while (!adivinado) {
    respuestaDelUsuario = int.parse(stdin.readLineSync()!);
    if (respuestaDelUsuario == 1) {
      limiteSuperior = interfaz.numeroAleatorio - 1;
      if (limiteSuperior > limiteInferior) {
        interfaz.generaNuevoNumeroAleatorio(limiteSuperior, limiteInferior);
        interfaz.presentaOpcionesAlUsuario();
      } else if (limiteInferior == limiteSuperior) {
        interfaz.elUsuarioHaAcertado(limiteSuperior);
      } else {
        interfaz.informaAlUsuarioQueLaHaCagado();
        break;
      }
    } else if (respuestaDelUsuario == 2) {
      limiteInferior = interfaz.numeroAleatorio + 1;
      if (limiteSuperior > limiteInferior) {
        interfaz.generaNuevoNumeroAleatorio(limiteSuperior, limiteInferior);
        interfaz.presentaOpcionesAlUsuario();
      } else if (limiteInferior == limiteSuperior) {
        interfaz.elUsuarioHaAcertado(limiteSuperior);
      } else {
        interfaz.informaAlUsuarioQueLaHaCagado();
        break;
      }
    } else if (respuestaDelUsuario == 3) {
      interfaz.elUsuarioHaAcertado(interfaz.numeroAleatorio);
      adivinado = true;
    } else {
      interfaz.respuestaINcorrectaDelUsuario();
    }
  }
}
