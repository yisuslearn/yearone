import 'dart:math';

class InterfazDeUsuario {
  int numeroAleatorio;
  Random generadorAleatorio;
  int numeroDeIntentos;

  InterfazDeUsuario(this.numeroAleatorio, this.generadorAleatorio, this.numeroDeIntentos);

  void informaAlUsuarioQueLaHaCagado() {
    print("Lo siento, no hay más opciones el número que pensabas es $numeroAleatorio");
  }

  void elUsuarioHaAcertado(int numeroAdivinado) {
    print("GEnial! he acertado el número que pensabas era $numeroAdivinado!! I am so happy");
    print("EL número de intentos has sido $numeroDeIntentos");
  }

  void respuestaINcorrectaDelUsuario() {
    print("Respuesta incorrecta, seleccione 1, 2 o 3 y pulse Enter");
  }

  void presentaOpcionesAlUsuario() {
    print("El número que te propongo ahora es $numeroAleatorio");
    print('Seleccione 1 si el número propuesto es muy alto');
    print('Seleccione 2 si el número propuesto es muy bajo');
    print('Seleccione 3 si el número propuesto es el que usted ha pensado');
  }

  void generaNuevoNumeroAleatorio(int limiteSuperior, int limiteInferior) {
    numeroAleatorio = generadorAleatorio.nextInt(limiteSuperior - limiteInferior + 1) + limiteInferior;

    numeroDeIntentos = numeroDeIntentos + 1;
  }
}
