//Dada una lista desorderada, ordernala entera de mayor a menor

import 'package:holamundo/puntuacion_maxima.dart';

void main() {
  final unsortedList = [PuntuacionMaxima("isra",5),PuntuacionMaxima("pepe",10),PuntuacionMaxima("EVA",17),PuntuacionMaxima("EMY",1),PuntuacionMaxima("TRISHA",22),PuntuacionMaxima("laura",7),PuntuacionMaxima("leo",9),PuntuacionMaxima("kenobi",6)];
  final sortedList = sortList(unsortedList);

  print('desordenada $unsortedList');
  print('ordenada $sortedList');
}

List<PuntuacionMaxima> sortList(List<PuntuacionMaxima> unsortedList) {
  List<PuntuacionMaxima> result = [];

  // 1) encuentra el más grande de la lista e imprímelo 22
  // 2) encuentra le mas grande y guardalo en result
  // 3) repetir 2) hasta que tengas todos ordenados de mayo a menor

  for (int i = 0; i < unsortedList.length; i++) {
    PuntuacionMaxima largestElement = PuntuacionMaxima("",0);  //  = unsortedList[0];
    for (int j = 0; j < unsortedList.length; j++) {
      if (largestElement.puntuacion < unsortedList[j].puntuacion && !result.contains(unsortedList[j])) {
        largestElement = unsortedList[j];

      }
    }
    result.add(largestElement);
  }
  return result;
}

