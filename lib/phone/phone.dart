class Phone {
  String screenSize;
  String batteryDuration;
  String numberOfLenses;

  //constructor de la clase
  // Nos va a construir INSTANCIAS (u OBJETOS) de la clase que sea
  // Phone(String pantalla, String bateria, String camaras) :
  //    screenSize = pantalla,
  //    batteryDuration = bateria ,
  //    numberOfLenses = camaras;

  Phone(this.screenSize, this.batteryDuration, this.numberOfLenses);


  void llamar(String numeroDeTelefono) {
    print('Te queda: $batteryDuration');
    print('Llamando a: $numeroDeTelefono');
  }
}


//añade el metodo llamar a numero de telefono
//declarar un método de una función no es más ni menos que delcarar una función
//dentro de una clase
