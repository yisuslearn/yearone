import 'phone.dart';

class Persona {
  //clase
  String nombre;
  int edad;
  double altura;
Phone telefono;

  Persona(this.nombre, this.edad, this.altura, this.telefono); //constructor

  void saludar() {
    //método
    print("Hola, mi nombre es $nombre y tengo $edad años.");
  }
  void llamarA(String nombre){
    telefono.llamar(nombre);

  }
}
