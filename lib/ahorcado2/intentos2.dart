import 'motor_grafico.dart';

class Intentos {
  List<String> letrasFallidas;
  int numeroIntentos;
  MotorGrafico motorGrafico;

  int numeroMaximoDeFallos = 9;

  Intentos(this.letrasFallidas, this.numeroIntentos, this.motorGrafico);

  void guardaFallo(String letra) {
    letrasFallidas.add(letra);
    numeroIntentos++;
  }

  bool letraYaUsada(String letra) {
    return letrasFallidas.contains(letra);
  }

  bool noEsteAhorcado() {
    return numeroIntentos < numeroMaximoDeFallos;
  }

  void imprimeIntentosRestantes() {
    print("Número de intentos que quedan: ${numeroMaximoDeFallos - numeroIntentos}");
    motorGrafico.dibujaNumeroDeFallos(numeroIntentos);
  }

  void imprimeLetrasUsadas() {
    print("Letras utilizadas: $letrasFallidas");
  }
}
