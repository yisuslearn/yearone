import 'dart:io';
import 'package:holamundo/ahorcado2/motor_grafico.dart';
import 'resultado2.dart';
import 'intentos2.dart';

void main() {
  print("¡Bienvenido al juego del ahorcado!");
  print("Ingresa una letra para adivinar la palabra.");

  Resultado solucion = Resultado("", []);
  Intentos intentos = Intentos([], 0, MotorGrafico());

  solucion.elijoPalabraAleatoria();
  solucion.muestraElNumeroDeLetrasDeLaSolucion();

  while (solucion.quedanLetrasPorAdivinar() && intentos.noEsteAhorcado()) {
    print("");
    intentos.imprimeLetrasUsadas();
    intentos.imprimeIntentosRestantes();

    print("¿Qué letra eliges?");
    String letraEscogida = stdin.readLineSync()!.toLowerCase();

    while (!solucion.validarEntrada(letraEscogida) || intentos.letraYaUsada(letraEscogida)) {
      print("Por favor, ingresa una sola letra válida.");

      if (intentos.letraYaUsada(letraEscogida)) {
        print("ya has probado con esa letra.");
      }

      letraEscogida = stdin.readLineSync()!.toLowerCase();
    }

    bool acierto = solucion.compruebaLaLetra(letraEscogida);

    if (acierto) {
      print("¡Has acertado una letra!");
    } else {
      intentos.guardaFallo(letraEscogida);
      print("La letra no está en la palabra.");
    }

    solucion.muestraLetrasAdivinadas();
  }

  if (!solucion.quedanLetrasPorAdivinar()) {
    print("");
    print("¡Felicidades, has adivinado la palabra!");
  } else {
    print("");
    print("Has agotado tus intentos, La palabra era: ${solucion.palabraEscogida}");
    MotorGrafico motorGrafico = MotorGrafico();
    motorGrafico.dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerechoYpiernaIqzYDcha();
  }
}
