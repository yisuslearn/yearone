import 'dart:math';

class Resultado {
  String palabraEscogida;
  List<String> letrasAdivinadas;

  Resultado (this.palabraEscogida ,this.letrasAdivinadas);

  void elijoPalabraAleatoria() {
    String texto =
        "El jugador adivinador completa la palabra, o adivina la palabra completa correctamente El otro jugador completa el diagrama: Este diagrama es , de hecho, diseñado para parecerse a un hombre ahorcado. A pesar de que han surgido debates sobre el gusto cuestionable de esta imagen todavía está en uso actualmente . Una alternativa común para maestros es dibujar un árbol de manzanas con diez manzanas, borrar o tachar las manzanas a medida que se agotan las adivinanzas. La naturaleza exacta de del diagrama difiere; algunos jugadores dibujan la horca antes de jugar y dibujan las partes del cuerpo del hombre tradicionalmente la cabeza, luego el torso, luego los brazos y las piernas de uno en uno). Algunos jugadores comienzan con sin esquema en absoluto, y elaboran los elementos individuales de la horca como parte del juego, dándole al jugador más posibilidades efectivas medida de adivinar. También pueden presentar variaciones los detalles en el hombre, lo que afecta el número de posibilidades. Algunos jugadores incluyen una cara en la cabeza, ya sea toda a la vez o una característica a la vez. A veces se aplican algunas modificaciones al juego para aumentar el nivel de dificultad, tales como la limitación de sobre las consonantes de alta frecuencia y las vocales. Otra alternativa es dar la definición de la palabra. Esto puede ser utilizado para facilitar el aprendizaje de una lengua extranjera.";
    final sinComas = texto.replaceAll(",", "");
    final sinPuntos = sinComas.replaceAll(".", "");
    final sinDosPuntos = sinPuntos.replaceAll(":", "");
    final sinPuntoYComa = sinDosPuntos.replaceAll(";", "");
    final enMinuscula = sinPuntoYComa.toLowerCase();
    final palabras = enMinuscula.split(" ");
    final sinDuplicadas = palabras.toSet();

    Random generadorDeAleatorios = Random();
    int posicionEscogida = generadorDeAleatorios.nextInt(sinDuplicadas.length);
    final sinDuplicadasList = sinDuplicadas.toList();
    palabraEscogida = sinDuplicadasList[posicionEscogida];
    letrasAdivinadas = List.generate(palabraEscogida.length, (_) => "_");
  }

  void muestraElNumeroDeLetrasDeLaSolucion() {
    print(letrasAdivinadas.join(' '));
  }

  bool compruebaLaLetra(String letra) {
    bool acierto = false;
    for (int i = 0; i < palabraEscogida.length; i++) {
      if (palabraEscogida[i] == letra) {
        acierto = true;
        letrasAdivinadas[i] = letra;
      }
    }
    return acierto;
  }

  bool quedanLetrasPorAdivinar() {
    return letrasAdivinadas.contains('_');
  }

  bool validarEntrada(String entrada) {
    return entrada.length == 1 && entrada.toLowerCase() != entrada.toUpperCase();
  }

  void muestraLetrasAdivinadas() {
    print(letrasAdivinadas.join(' '));
  }
}



