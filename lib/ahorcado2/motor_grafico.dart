
class MotorGrafico {


  void dibujaNumeroDeFallos(int numeroIntentos) {

    if (numeroIntentos == 1) {
      dibujaPaloGordo(0);
    } else if (numeroIntentos == 2) {
      dibujaTravesanno();
      dibujaPaloGordo(0);
    } else if (numeroIntentos == 3) {
      dibujaTravesanno();
      dibujaPaloGordoConSoga();
    } else if (numeroIntentos == 4) {
      dibujaTravesanno();
      dibujaPaloGordoConSogaYCabeza();
    } else if (numeroIntentos == 5) {
      dibujaPaloGordoConSogaYCabezaYcuerpo();
    } else if (numeroIntentos == 6) {
      dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdo();
    } else if (numeroIntentos == 7) {
      dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerecho();
    } else if (numeroIntentos == 8) {
      dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerechoYpiernaIqz();
    } else if (numeroIntentos == 9) {
      dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerechoYpiernaIqzYDcha();
    }
  }

  void dibujaPaloGordo(int cuantosHayQueQuitar) {
    int maxSize = 7;
    for (int i = 0; i <= maxSize - cuantosHayQueQuitar; i++) {
      print('|');
    }
  }

  void dibujaTravesanno() {
    print('--------');
  }

  void dibujaPaloGordoConSoga() {
    int cuantosPalosGordosHayQueQuitar = dibujaSoga();
    dibujaPaloGordo(cuantosPalosGordosHayQueQuitar);
  }

  void dibujaPaloGordoConSogaYCabeza() {
    int sogaConCabeza = dibujaSogaConCabeza();
    dibujaPaloGordo(sogaConCabeza);
  }

  int dibujaSoga() {
    print('|      |');
    print('|      |');
    return 2;
  }

  void dibujaPaloGordoConSogaYCabezaYcuerpo() {
    dibujaTravesanno();
    int cuantosDeSogaConCabeza = dibujaSogaConCabeza();
    print("|      |");

    dibujaPaloGordo(cuantosDeSogaConCabeza + 1);
  }

  int dibujaSogaConCabeza() {
    int cuantosDeSoga = dibujaSoga();
    print("|      O");
    return cuantosDeSoga + 1;
  }

  void dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdo() {
    dibujaTravesanno();
    int cuantosDeSoga = dibujaSogaConCabeza();
    print("|     /|");
    dibujaPaloGordo(cuantosDeSoga + 1);
  }

  void dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerecho() {
    int cuantosDeTrenSuperior = dibujaHastaTrenSuperior();
    dibujaPaloGordo(cuantosDeTrenSuperior);
  }

  void dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerechoYpiernaIqz() {
    int cuantosDeTrenSuperior = dibujaHastaTrenSuperior();
    print("|     / ");
    dibujaPaloGordo(cuantosDeTrenSuperior + 1);
  }

  void dibujaPaloGordoConSogaYCabezaYCuerpoYBrazoIzquierdoYDerechoYpiernaIqzYDcha() {
    int cuantosHastaSuperior = dibujaHastaTrenSuperior();
    print("|     / \\");
    dibujaPaloGordo(cuantosHastaSuperior + 1);
          }

  int dibujaHastaTrenSuperior() {
    dibujaTravesanno();
    var cuantosDeSoga = dibujaSogaConCabeza();
    print("|     /|\\");
    return cuantosDeSoga + 1;
  }
}
