void main() {
  List<int> puntuaje = [22, 20, 5, 4];
  // List<int> puntuaje = [22, 50, 5, 4];
  List<int> conUnoMasYOrdenada = sortedAdd(puntuaje, 3);
  print(conUnoMasYOrdenada);
  print([22, 20, 5, 4]);
}

List<int> sortedAdd(List<int> puntuajeSiempreOrdenado, int elemento) {
  List<int> result = [];
  // int newElementPosition = 0; //index me sirve para para almacenar la posición de la inserción del nuevo elemento

  if (puntuajeSiempreOrdenado.isEmpty) {
    result.add(elemento);
  } else {
    bool noEstaMetido = true;
    for (int i = 0; i < puntuajeSiempreOrdenado.length; i++) {
      if (noEstaMetido && elemento > puntuajeSiempreOrdenado[i]) {
        result.add(elemento);
        noEstaMetido = false;
      }
      result.add(puntuajeSiempreOrdenado[i]);
    }
    if (noEstaMetido = true) {
      result.add(elemento);
    }
  }
// result.insert(newElementPosition, elemento);

  return result;
}
