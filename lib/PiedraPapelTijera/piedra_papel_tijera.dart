String resultadoDeLaApuesta(String opcionJugador, String opcionComputadora) {
  if (opcionJugador == opcionComputadora) {
    return 'Empate';
  } else if (jugadorGana(opcionJugador, opcionComputadora)) {
    return 'Has ganado!! $opcionJugador gana a $opcionComputadora';
  } else {
    return "Has perdido, $opcionComputadora gana a $opcionJugador";
  }
}

bool jugadorGana(String opcionJugador, String opcionComputadora) {
  if (opcionJugador == 'piedra' && opcionComputadora == 'tijera') {
    return true;
  } else if (opcionJugador == 'tijera' && opcionComputadora == 'papel') {
    return true;
  } else if (opcionJugador == 'papel' && opcionComputadora == 'piedra') {
    return true;
  }
  return false;
}
