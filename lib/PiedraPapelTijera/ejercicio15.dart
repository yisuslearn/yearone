//Make a two-player Rock-Paper-Scissors game against computer.
// Ask for player’s input, compare them, print out a message to the winner.

import 'dart:io';
import 'dart:math';
import 'piedra_papel_tijera.dart';
import 'borrar.dart';

void main() {
  bool juegoIndefinido = true;
  print('Juguemos a piedra, papel o tijera:');
  while (juegoIndefinido) {
    print('Introduce tu opción elegida o "exit" para salir del programa');
    String opcionJugador = stdin.readLineSync()!.toLowerCase();

    if (opcionJugador == 'exit') {
      juegoIndefinido = false;
    } else {

    opcionJugador = validarEntradaDeUsuario(opcionJugador);
    print(opcionJugador);

      String opcionComputadora =
          ['piedra', 'papel', 'tijera'][Random().nextInt(3)];

      print('el ordenador eligió $opcionComputadora');

      print(resultadoDeLaApuesta(opcionJugador, opcionComputadora));
    }
  }
}
