import 'dart:io';

String validarEntradaDeUsuario(String opcionJugador){
  while (opcionJugador != "piedra" &&
      opcionJugador != "papel" &&
      opcionJugador != "tijera") {
    print('escribe piedra, papel o tijera cabesa');
    opcionJugador = stdin.readLineSync()!.toLowerCase();
  }
  return opcionJugador;
}