//Dada una lista desorderada, ordernala entera de mayor a menor

void main() {
  final unsortedList = [8, 5, 10, 17, 1, 22, 7, 9, 6];
  final sortedList = sortList(unsortedList);

  print('desordenada $unsortedList');
  print('ordenada $sortedList');
}

List<int> sortList(List<int> unsortedList) {
  List<int> result = [];

  // 1) encuentra el más grande de la lista e imprímelo 22
  // 2) encuentra le mas grande y guardalo en result
  // 3) repetir 2) hasta que tengas todos ordenados de mayo a menor

  for (int i = 0; i < unsortedList.length; i++) {
    int largestElement = 0;  //  = unsortedList[0];
    for (int j = 0; j < unsortedList.length; j++) {
      if (largestElement < unsortedList[j] && !result.contains(unsortedList[j])) {
        largestElement = unsortedList[j];

      }
    }
    result.add(largestElement);
  }
  return result;
}

