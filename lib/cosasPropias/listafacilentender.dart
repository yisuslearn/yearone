void main() {
  List<String> colores = ['rojo', 'verde', 'azul'];
  String primerColor = colores[0]; //Acceso a primer elemento
  print(primerColor);

  List<int> numeros = [1,2,3,4,5];
  print(numeros);
  numeros[2] = 10; //modifica tercer elemento
  print(numeros);

  List<String> frutas = ['manzana','banana','naranja'];
  print(frutas.length);
  frutas.add('uva');
  print(frutas);
  frutas.remove('banana');
  print(frutas);
  String variableimprimeContieneNarajna = "la lista contiene naranja";
  bool contieneNaranja = frutas.contains('naranja');
  print(contieneNaranja); //imprime por defecto true
  print(variableimprimeContieneNarajna);

}

//Operaciones comunes con listas:
// Dart proporciona una variedad de métodos y operaciones para trabajar con listas. Algunos ejemplos son:
//
//     length: Devuelve la longitud de la lista.
//     add(): Agrega un elemento al final de la lista.
//     remove(): Elimina un elemento de la lista.
//     contains(): Verifica si un elemento está presente en la lista.