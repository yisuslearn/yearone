void main() {
  List<int> numeros = [1, 2, 3, 4, 5];

  numeros[2] = 10; // Modifica el tercer elemento y le asigna el valor 10

  print(numeros); // Imprime el arreglo modificado
}
