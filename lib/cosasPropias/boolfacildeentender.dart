//Operadores de comparación y lógicos:
// Los valores booleanos se utilizan comúnmente en expresiones que involucran
// operadores de comparación y lógicos. Algunos ejemplos de operadores de
// comparación son:
//     ==: Igual a
//     !=: Diferente de
//     <: Menor que
//     >: Mayor que
//     <=: Menor o igual que
//     >=: Mayor o igual que

void main(){
  int x = 5;
  int y = 10;

  bool resultado = x < y;
  print(resultado);  // Imprime false
String pruebaResultado;
  if (resultado) {
    pruebaResultado = "verdadero";
  } else {
    pruebaResultado = 'Falso';
  }
  print(pruebaResultado); //probando otra forma añadiendo if - else

//Además de los operadores de comparación, puedes utilizar operadores lógicos
// para combinar expresiones booleanas. Los operadores lógicos más comunes son:
//
//     &&: AND lógico (y)
//     ||: OR lógico (o)
//     !: NOT lógico (no)

  bool a = true;
  bool b = false;
  bool c = true;

  bool resultado2 = a && (b || c);
  print(resultado2);  // Imprime true

  //Los valores booleanos se utilizan en condiciones de control, como las
  // instrucciones if, while, for, entre otras. Estas estructuras de control
  // toman decisiones basadas en el valor booleano de una expresión.
  //
  // Ejemplo que utiliza un valor booleano en una estructura if:

  bool esDiaSoleado = true;

  if (esDiaSoleado) {
    print("Vamos a la playa!");
  } else {
    print("Mejor quedémonos en casa.");
  }

}